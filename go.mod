module gitlab.com/kennethsohyq/school/university/fyp/worker-structured-questions/v2

go 1.15

require (
	github.com/go-redis/redis/v8 v8.2.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.7.0
)