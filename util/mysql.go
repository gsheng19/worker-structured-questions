package util

import (
	"database/sql"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kennethsohyq/school/university/fyp/worker-structured-questions/v2/models"
	"strconv"
	"strings"
)

func ConnectMySQL() *sql.DB {
	username := GetEnvDefault("MYSQL_USER", "")
	password := GetEnvDefault("MYSQL_PASSWORD", "")
	port := GetEnvDefault("MYSQL_PORT", "3306")
	host := GetEnvDefault("MYSQL_HOST", "localhost")
	name := GetEnvDefault("MYSQL_DATABASE", "aasp")

	db, err := sql.Open("mysql", username+":"+password+"@tcp("+host+":"+port+")/"+name)
	if err != nil {
		panic(err)
	}

	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	return db
}

// Get Submission attempt from DB and push to Redis
func GetSubmissionAttemptAsRedis(id string) (*models.Answer, []models.Keywords) {
	db := ConnectMySQL()
	defer db.Close()

	status := models.Answer{Response: ""}
	stmtGen, err := db.Prepare("SELECT qq.attemptid, qn.questionid, qq.answer, qn.id FROM quiz_attempt_qn qq JOIN quiz_questions qn ON qq.questionid=qn.id WHERE qq.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	defer stmtGen.Close()
	var aid int
	var qid int
	var qnid int

	err = stmtGen.QueryRow(id).Scan(&aid, &qid, &status.Response, &qnid)
	status.ResponseLowered = strings.ToLower(status.Response)
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}

	// Get max score
	stmtScore, err := db.Prepare("SELECT q.maxscore FROM quiz_questions qn JOIN question q ON qn.questionid=q.id WHERE qn.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	defer stmtScore.Close()
	var maxScore sql.NullString
	err = stmtScore.QueryRow(qnid).Scan(&maxScore)
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	if maxScore.Valid {
		status.MaxScoreQn = json.Number(maxScore.String)
	} else {
		status.MaxScoreQn = "0"
	}

	// Get question count
	keyCnt, err := db.Prepare("SELECT length(id) FROM question_structured_keywords WHERE questionid=?")
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	defer keyCnt.Close()
	var qCount int
	err = keyCnt.QueryRow(qid).Scan(&qCount)
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}

	mScore,_ := status.MaxScoreQn.Float64()
	log.Debugf("Max Score: %f | Qn Cnt: %d", mScore, qCount)

	// Get keywords
	keywordRows, err := db.Query("SELECT keyword,occurance,score,casesensitive FROM question_structured_keywords WHERE questionid=?", qid)
	if err != nil {
		log.Errorln(err.Error())
		return &status, nil
	}
	defer keywordRows.Close()

	var keywords []models.Keywords

	for keywordRows.Next() {
		var kw models.Keywords
		var caseSenInt int
		var score sql.NullString
		err = keywordRows.Scan(&kw.Word, &kw.Occurrence, &score, &caseSenInt)
		if err != nil {
			log.Errorln(err.Error())
			continue
		}

		if score.Valid {
			kw.Score = json.Number(score.String)
		} else {
			// divide and round up
			kw.Score = json.Number(strconv.FormatFloat(mScore / float64(qCount), 'e', 2, 64))
			log.Debugln("Score split assigned to keyword as " + kw.Score)
		}
		kw.CaseSensitive = caseSenInt == 1

		keywords = append(keywords, kw) // Add to keywords
	}

	return &status, keywords
}

// Update Submission Attempt in DB for structured questions
func UpdateSubmissionAttempt(id string, score float64) bool {
	db := ConnectMySQL()
	defer db.Close()

	stmtMaxScore, err := db.Prepare("SELECT qn.maxscore FROM question qn JOIN quiz_questions qq ON qq.questionid = qn.id JOIN quiz_attempt_qn qa ON qa.questionid = qq.id WHERE qa.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	defer stmtMaxScore.Close()

	stmtUpdateTestAttempt, err := db.Prepare("UPDATE quiz_attempt_qn SET score=?, ismarked=1 WHERE id=?")
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	defer stmtUpdateTestAttempt.Close()

	var maxscore json.Number
	err = stmtMaxScore.QueryRow(id).Scan(&maxscore)
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	maxscoreFloat, _ := maxscore.Float64()

	if score > maxscoreFloat {
		score = maxscoreFloat
	}

	log.Debugf("Score: %f | Submit: %f", maxscoreFloat, score)

	_, err = stmtUpdateTestAttempt.Exec(score, id)
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	return true
}
